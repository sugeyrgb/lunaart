const CatalogComponent = Vue.component("catalog",{
	template: 
	`	<div class='nav-padder'>
			<b-container fluid v-if="catalogs.length">
				<b-row class='padding'>
		          <b-col>
		            <h4 style="color: #1E2446;" class="font-boldy elmessiri gold-flat-text component-title">Catalog</h4>
		          </b-col>
		        </b-row>
				<b-row class='padding'>
					<b-col sm=12 offset-md=2 md=2 class="text-center main-catalog" >
						<b-img :src="catalogs[0].img" fluid></b-img> <br>
						<button class="btn-viewCatalog" @click="openPdf(catalogs[0].pdf)">VIEW CATALOG</button>
						<h5 class='gold-flat-text'>Coastline</h5>
					</b-col>

					<b-col sm=12 offset-md=1 md=2 class="text-center main-catalog">
						<b-img :src="catalogs[1].img" fluid></b-img> <br>
						<button class="btn-viewCatalog" @click="openPdf(catalogs[1].pdf)">VIEW CATALOG</button>
						<h5 class='gold-flat-text'>Progressive</h5>
					</b-col>

					<b-col sm=12 offset-md=1 md=2 class="text-center main-catalog" >
						<b-img :src="catalogs[2].img" fluid></b-img> <br>
						<button class="btn-viewCatalog" @click="openPdf(catalogs[2].pdf)">VIEW CATALOG</button>
						<h5 class='gold-flat-text'>Timeless</h5>
					</b-col>
				</b-row>

				<b-row>
					<b-col sm=12 offset-md=3 md=2 class="text-center main-catalog" >
						<b-img :src="catalogs[3].img" fluid></b-img> <br>
						<button class="btn-viewCatalog" @click="openPdf(catalogs[3].pdf)">VIEW CATALOG</button>
						<h5 class='gold-flat-text'>Traditional</h5>
					</b-col>

					<b-col sm=12 offset-md=2 md=2 class="text-center main-catalog" >
						<b-img :src="catalogs[4].img" fluid></b-img> <br>
						<button class="btn-viewCatalog" @click="openPdf(catalogs[4].pdf)">VIEW CATALOG</button>
						<h5 class='gold-flat-text'>Geometric</h5>
					</b-col>
				</b-row>
			</b-container>

			<b-container v-else>
				<b-row>
					<b-col>
						<span>Loading...</span>
					</b-col>
				</b-row>
			</b-container>
		</div>
	  `,
	data() {
		return {
			catalogs: [],
		}
	},
	beforeCreate(){
	  var main = this;
      NProgress.start();
      main.catalogs = [];
      axios({
        method: 'get',
        url: 'lpage?g=catalogs',
      }).then(function (response) {
          //handle success
          console.log(response);
          console.log(response.data);

          let call = response.data.call;

           switch(call){

             case true:
               if(response.data.hasOwnProperty("info")){
                  main.catalogs = response.data.info;
               }else{
                 main.catalogs = [];
               }
              
             break;

             default:
               vd.toastError();
             break;
           }

           main.loading = false;
      }).catch(function (response) {
          //handle error
          console.log(response);
      }).then(function () {
        NProgress.done();
      }); 
	},
	update(){

	},
	methods: {
		openPdf(url){
			window.open(url);
		}
	}
});
