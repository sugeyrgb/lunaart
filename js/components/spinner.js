const SpinnerComponent = Vue.component("l-spinner",{
	props: {
		prevClass: {
			default: "spinner-border"
		},
		color: {
			default: "red-text"
		}
	},
	data:function(){
		return {}
	},
 	template: `<div>
					<div :class="prevClass+' '+color" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>`,
	created(){

	}
});