const lGalleryComponent = Vue.component("lgallery",{
	template: 
	`<div class="nav-padder">
		
		<b-container fluid v-if=loading>
			<b-row v-if="galleries.length">
				<b-col class="text-center padding">
					<l-spinner color="luna-text-gold"></l-spinner>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid v-else>
			<!--
			<b-container class="padding">
				
				<b-row>

					<b-input-group class="mt-3">
				       <b-form-input v-model="sorting" @input="searchMat" placeholder="SEARCH IMAGE" autocomplete=off />
				    </b-input-group>

				</b-row>


			</b-container>
			

			<b-container fluid v-if="sorting">
				
				<b-row>

					<b-col sm=3  v-for="m in sortingM">
						<b-card no-body :img-src="m.img" img-alt="Image" :class="gallery-card" img-top>
					    </b-card>
					</b-col>

				</b-row>


			</b-container>
			-->
			<b-container fluid>
				<!--
				<b-tabs class="nav-justified" content-class="mt-3" v-if="galleries.length">
					<b-tab title="Kitchen" active>
						
						<b-container fluid>
							<b-row>
								<b-col sm=3  v-for="m in kitchen"  >
									<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card"  img-top>
								    </b-card>
								</b-col>
							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Bathroom">
						
						<b-container fluid>	
							<b-row>
							
								<b-col sm=3  v-for="m in bathroom" >
									<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card" img-top>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Comercial space">
						
						<b-container>	
							<b-row>
							
								<b-col sm=3  v-for="m in comercialSpace" >
									<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card" img-top>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Living space">
						
						<b-container fluid>	
							<b-row>
							
								<b-col sm=3  v-for="m in livingSpace" >
									<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card" img-top>
								    </b-card>
									
								</b-col>

							</b-row>
						</b-container>

					</b-tab>
				</b-tabs>
				-->

				<b-row>
		          <b-col>
		            <h4 style="color: #1E2446;" class="font-boldy elmessiri gold-flat-text component-title">Gallery</h4>
		          </b-col>
		        </b-row>
					
				<b-row v-if="galleries.length">
					<b-col class="text-center">
						<b-form-group>
					      <b-form-checkbox-group
					        v-model="selected"
					        :options="options"
					        v-on:change="sortGallery"
					      ></b-form-checkbox-group>
					    </b-form-group>
				    </b-col>
				</b-row>
					
				<b-row v-if="showSort.length">
					<b-col sm=2 v-for="(m,index) in showSort" >
						
						<div v-viewer class="images clearfix gallery-card">
						    <template>
<<<<<<< HEAD
						    	<a :href="m.img" :download="m.title+'.jpg'"><i class="fas fa-download"></i></a>
						      <b-img fluid :src="m.img" class="image" :key="m.img"/>
						    </template>
						</div>
						
=======
						    	<a :href="m.img" :download="m.title"><i class="fas fa-arrow-down"></i></a>
						      <b-img fluid :src="m.img" class="image" :key="m.img"/>
						    </template>
						</div>
						show () {
	      const viewer = this.$el.querySelector('.images').$viewer
	      viewer.show()
	    }
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
					</b-col>
				</b-row>
			</b-container>
		 </b-container>
	</div>
	  `,
	data() {
		return {
			galleries: [],

			kitchen: [],
			bathroom: [],
			comercialSpace: [],
			livingSpace: [],

			showSort: [],

			loading: true,
			sorting: "",
			sortingM: [],

			selected: [], // Must be an array reference!
	        options: [
	          { text: 'KITCHEN', value: 1 },
	          { text: 'BATHROOM', value: 2 },
	          { text: 'COMMERCIAL SPACE', value: 3 },
	          { text: 'LIVING SPACE', value: 4 }
	        ],

			vd: new VueDiana()
		}
	},
	mounted(){
		console.log(this.galleries);
		console.log(this.sortingM);
	},
	created(){

		/*

	<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card" img-top>
							<a :href="m.img" :download="m.title"><i class="fas fa-arrow-down"></i></a>
					    </b-card>
		*/

		this.$root.$data.gallery = [];
		var main = this;
		main.loadGallery();

		console.log("Hi")

		window.addEventListener("reloadGallery",function(){
			main.loadGallery();
			main.$forceUpdate();
		});
	},
	updated(){
		console.log(this.galleries);
		console.log(this.sortingM);
	},
	methods: {
		sortGallery(e){
			console.log(e)

			var main = this;
			setTimeout(function() {
				main.showSort = main.galleries.filter(function(index) {
					console.error(index)
					for(var checkbox in main.selected){
						console.warn(main.selected[checkbox]);
						if(index.category == main.selected[checkbox]){
							return index;
						}
					}
					
				});
			}, 100);
			

			main.$forceUpdate();
		},
		searchMat(){
			var main = this;
			
			this.sortingM = this.galleries.filter(function(index) {
				console.log(index.title.toLowerCase())
				console.log(main.sorting.toLowerCase())
				if(index.title.toLowerCase().match(main.sorting.toLowerCase())){
					return index;
				}
				
			});
		},
		loadGallery(){
			//asset?r=designpattern&a=da
			 var main = this;

			 main.loading = true;

			 main.galleries = [];

			 main.kitchen = [];
			 main.bathroom = [];
			 main.comercialSpace = [];
			 main.livingSpace = [];

			  axios({
		        method: 'get',
		        url: 'gallery?g=a',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		          main.errors = [];

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  	main.galleries = response.data.info;

		                  	main.sortingM = main.galleries;

							for(var m in main.galleries){
						  
						      	main.galleries[m].category = parseInt(main.galleries[m].category);
						      	
						      	switch(main.galleries[m].category){
							  		case 1:
							  			main.kitchen.push(main.galleries[m]);
							  		break;

							  		case 2:
							  			main.bathroom.push(main.galleries[m]);
							  		break;

							  		case 3:
							  			main.comercialSpace.push(main.galleries[m]);
							  		break;

							  		case 4:
							  			main.livingSpace.push(main.galleries[m]);
							  		break;

							  		default:
							  			console.log("Nu")
							  		break;
							  	}
					        }
		               }else{
		                  main.galleries = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		           main.loading = false;
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });

		    main.$forceUpdate();
		},
	}
});

