const PackingComponent = Vue.component("packing",{
	template: 
	`
	  <div class='cs-container'>

		<b-container fluid>
			<b-row>
				<b-col sm=12 md=6>
					<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
				</b-col>

				<b-col sm=12 md=6>
					<b-form-input v-model="search" @input="sortPackingList()" placeholder="ORDER NUMBER" autocomplete="off" />
			
				</b-col>
			</b-row>
			
			<b-row class="text-center padding" v-if='loading'>
              <b-col>
                <l-spinner color="luna-text-gold"></l-spinner>
              </b-col>
            </b-row>
			
			<div v-if="!sortPacking.length">

				<b-row>
					<b-col sm=12 v-if="packings.length">
						<b-col class="padding">

			                <div class="table-responsive">
			                    <table class="table table-hover text-center" style="width: 100%!important">
			                      <thead>
			                        <tr>
			                          <th v-for="f in infields" scope="col">{{ f }}</th>
			                        </tr>
			                      </thead>
			                      <tbody>
			                        <tr v-for="pac in packings">
			                          <th scope="row">#{{ pac.number }}</th>
			                       	  <td>{{ pac.track }}</td>
			                       	  <td>{{ pac.fdated }}</td>
			                       	  <td>{{ pac.boxes }}</td>
			                       	  <td>{{ pac.weight }}Lbs</td>
			                       	  <td>{{ pac.width }}" x {{ pac.height }}"</td>
			                       	  <td>{{ statusDisplay[pac.status] }}</td>
			                       	  <td>{{ pac.carrier }}</td>
			                       	  <td>{{ pac.origin }}</td>
			                       	  <td>{{ pac.destiny }}</td>
			                        </tr>
			                      </tbody>
			                    </table>
			                </div>

		                </b-col>
					</b-col>
				
					<b-col sm=12 class="text-center" v-else>
						<p class="text-center" >There's no packing lists</p>
					</b-col>
				</b-row>
			
			</div>

			<div v-else>
				<b-row>
					<b-col sm=12 v-if="sortPacking.length">
						<b-col class="padding">

			                <div class="table-responsive">
			                    <table class="table table-hover text-center" style="width: 100%!important">
			                      <thead>
			                        <tr>
			                          <th v-for="f in infields" scope="col">{{ f }}</th>
			                        </tr>
			                      </thead>
			                      <tbody>
			                        <tr v-for="pac in sortPacking">
			                          <th scope="row">#{{ pac.number }}</th>
			                       	  <td>{{ pac.track }}</td>
			                       	  <td>{{ pac.fdated }}</td>
			                       	  <td>{{ pac.boxes }}</td>
			                       	  <td>{{ pac.weight }}Lbs</td>
			                       	  <td>{{ pac.width }}" x {{ pac.height }}"</td>
			                       	  <td>{{ statusDisplay[pac.status] }}</td>
			                       	  <td>{{ pac.carrier }}</td>
			                       	  <td>{{ pac.origin }}</td>
			                       	  <td>{{ pac.destiny }}</td>
			                        </tr>
			                      </tbody>
			                    </table>
			                </div>

		                </b-col>
					</b-col>
				
					<b-col sm=12 class="text-center" v-else>
						<p class="text-center" >There's no match packing lists</p>
					</b-col>
				</b-row>
			</div>
			
		</b-container>
	  </div>`,
	data() {
		return {

			search: "",

			statusOpt: [
				{value: 1, text: "Preparing"},
				{value: 2, text: "Shipped"},
				{value: 3, text: "In transit"},
				{value: 4, text: "Delivered"},
				{value: 5, text: "Canceled"},
			],

			statusDisplay: ["Preparing", "Shipped", "In transit", "Delivered", "Canceled"],

			orders: [],

			loading: true,

			infields: ["ORDER", "TRACKING NUMBER", "DATE", "TOTAL BOXES", "TOTAL WEIGHT", "SIZE", "STATUS", "CARRIER", "ORIGIN", "DESTINY"],

			packings: [],

			pack: [],

			states: [],

			invErrors: [],

			sortPacking: [],

			vd: new VueDiana()


		}
	},
	created(){
		this.$root.$data.componentTitle = "Packing lists";
		this.searchPacking();

		this.searchPacking();
		let main = this;
		var timeQuery = setInterval(function(){
			main.searchPacking();
		},60000);

		axios.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8303e1d831e89cb8af24a11f2fa77353c317e408/states_titlecase.json').then(function(response){
	      let lalas = response.data;
	      console.log(lalas)
	      console.log(main.states);
	      for(var key in response.data){
	      	var temp = {
	      		text: response.data[key].name,
	      		value: response.data[key].abbreviation,
	      	}

	      	main.states.push(temp);
	      }
	      console.log(main.states);
	    }).catch(function(error){
	      console.log(error);
	    });
	},
	update(){

	},
	methods: {
		sortPackingList(){
			let main = this;
			this.sortPacking = this.packings.filter(function(index) {
				
				if(index.number.toLowerCase().match(main.search.toLowerCase()) || index.fdated.toLowerCase().match(main.search.toLowerCase())){
					return index;
				}

				console.log(index.number.toLowerCase())
				console.log(main.search.toLowerCase())
				
			});
		},
		searchPacking(){
			var vd = new VueDiana();
			var main = this;
			main.loading = true;

			main.packings = [];

			axios({
				method: 'get',
				url: 'packing-lists?g=m',
			}).then(function (response) {
			  //handle success
			  console.log(response);
			  console.log(response.data);

			  let call = response.data.call;

			   switch(call){

			     case true:
			     	if(response.data.info.length){
			     		main.packings = response.data.info;
			     	}else{
			     		main.packings = []
			     	}
			     	
			     break;

			     case false:
			       switch(response.data.errors){

			         default:
			           vd.toastError();
			         break;
			       }
			     break;
			     
			     default:
			       vd.toastError();
			     break;
			   }
			}).catch(function (response) {
			  //handle error
			  console.log(response);
			}).then(function () {
				main.loading = false;
			});  
		},
	}
});
