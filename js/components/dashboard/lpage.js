const lPageComponent = Vue.component("lpage",{
	template: 
	`
	 <div class='cs-container'>
		<b-container fluid>
			<b-row>
              <b-col sm=12 >
                <h4 class="component-title padding">{{ $root.$data.componentTitle }}</h4>
              </b-col>
            </b-row>

			<b-tabs class="nav-justified" content-class="mt-3">
				<b-tab title="Home page" active>
					<b-button v-b-toggle.collapse-1 variant="primary">SLIDES</b-button>
					<b-button v-b-toggle.collapse-2 variant="primary">CARDS</b-button>
					<b-button v-b-toggle.collapse-3 variant="primary">VIDEO</b-button>

					<b-collapse id="collapse-1" class="mt-2">
						<b-row>
							<b-col>
								<b-tabs card>

								<b-row v-if="errors.length" class='padding'>
		                          <b-col sm=12 v-for="error in errors">
		                            <b-alert variant="danger" show>{{ error.message }}</b-alert>
		                          </b-col>
		                        </b-row>
						        <b-tab v-for="(item,i) in tabs" :key="i" :title="(item.title == '' ? 'Untitled slide' : item.title)">

						          <div>
									 <b-row>
									 	<b-col class='text-right padding'>
											<b-button size="sm" class="float-right" @click="uploadSlide(i)">
									            SAVE
									        </b-button>
						     			</b-col>
						     			<b-col class='text-right padding'>
											<b-button size="sm" variant="danger" class="float-right" @click="closeTab(i)">
									            DELETE SLIDE
									        </b-button>
						     			</b-col>
									 </b-row>

									 <b-row>
										<b-col>
											<label :for="'file-input-'+i">
			                                	<b-img :src="tabs[i].img" fluid></b-img>
			                                	
			                                </label>
		                               		<input :id="'file-input-'+i" v-on:change="setImage($event,i)" class="hidden" type="file" accept="image/*" />
											
										</b-col>

										<b-col sm=12>
											<b-form-group
												label="TITLE"
												>
												<b-form-input placeholder='SET TITLE' v-model="item.title" autocomplete="off" trim></b-form-input>
											</b-form-group>
									 	</b-col>

									 	<b-col sm=12>
											<b-form-group
												label="EXTRA CONTENT"
												>
												<b-form-textarea
													v-model="item.content"
													placeholder="PLACE SOMETHING..."
													rows="3"
													max-rows="3"
												></b-form-textarea>
											</b-form-group>
									 	</b-col>
									 </b-row>
						          </div>
						          
						          
						        </b-tab>

						        <template slot="tabs">
						          <b-nav-item @click.prevent="newTab" href="#"><b>+</b></b-nav-item>
						        </template>

						        <div slot="empty" class="text-center text-muted">
						          There are no slides<br>
						          Create a new slide using the <b>+</b> button above.
						        </div>
						      </b-tabs>
							</b-col>
						</b-row>
					</b-collapse>

					<b-collapse id="collapse-2" class="mt-2">
						<b-row>
							<b-col>
						
								<b-col>
									<label for="file-collection" style="width:100%; height: 150px;">
										COLLECTIONS
										<b-card :class="'card-home'" :img-src="cards[0].img" class="text-center" style="height: 150px;">
											
										</b-card>
									</label>

									<input id="file-collection" @input="displayImg($event,0)" class="hidden" type="file" accept="image/*" />
								</b-col>

								<b-col>
									<label for="file-material"  style="width:100%; height: 150px;">
										MATERIALS
										<b-card :class="'card-home'" :img-src="cards[1].img" class="text-center" style="height: 150px;">	
											
										</b-card>
									</label>

									<input id="file-material" @input="displayImg($event,1)" class="hidden" type="file" accept="image/*" />
								</b-col>
							</b-col>

							<b-col>
								<label for="file-shop"  style="width:100%; height: 300px;">
									SHOP NOW
									<b-card :class="'card-home card-home-bigg'" :img-src="cards[2].img" class="text-center" style="height: 330px">
										
									</b-card>
								</label>

								<input id="file-shop" @input="displayImg($event,2)" class="hidden" type="file" accept="image/*" />
							</b-col>

							<b-col sm=12>
								<label for="file-avai" style="width:100%; height: 150px;">
									AVAILABILITY
									<b-card :class="'card-home'" :img-src="cards[3].img" class="text-center" style="height: 150px">
										
									</b-card>
								</label>

								<input id="file-avai" @input="displayImg($event,3)" class="hidden" type="file" accept="image/*" />
							</b-col>
						</b-row>

						<b-row>
							<b-col class="text-right">
								<b-button @click="uploadCards">SAVE</b-button>
							</b-col>
						</b-row>
					</b-collapse>

					<b-collapse id="collapse-3" class="mt-2">
						<b-row>
							<b-col>
								<b-form-group
									label="HOME PAGE VIDEO"
								>
									<b-form-file
										v-model="video.file"
										placeholder="Choose a file..."
										drop-placeholder="Drop file here..."
										accept="video/mp4"
									></b-form-file>
								</b-form-group>

							</b-col>
						</b-row>

						<b-row class="padding">
							<b-col class="text-right">
								<b-button @click="uploadVideo">UPLOAD</b-button>
							</b-col>
						</b-row>
					</b-collapse>
				</b-tab>
				
				<b-tab title="catalogs">
					<b-form-group label="PDF FILE">
						<b-form-file
							v-model="catalog.file"
							placeholder="Choose a file..."
							drop-placeholder="Drop file here..."
							accept="file/pdf"
						></b-form-file>
					</b-form-group>


					<b-form-group label="CATALOG THUMBNAIL">
						<b-form-file
							v-model="catalog.img"
							placeholder="Choose a file..."
							drop-placeholder="Drop file here..."
							accept="img/*"
						></b-form-file>
					</b-form-group>

					<b-form-group label="CATEGORY">
						<b-form-select v-model="catalog.section" :options="catalogOpt"></b-form-select>
					</b-form-group>
					
					<b-row class="padding">
						<b-col class="text-right">
							<b-button @click="uploadCatalog">UPLOAD</b-button>
						</b-col>
					</b-row>
				</b-tab>

				<b-tab title="config">
					<b-row>
						<b-col sm=12>
							<b-form-input v-model="sortEmail" @input="sortEmails()" placeholder="SEARCH EMAIL" autocomplete="off" />
						</b-col>

					    <b-col sm=12 class="padding">
					        <b-collapse id="collapse-6">
					          <b-card>
					            
					            <b-form-group
									label="TITLE"
								>
									<b-form-input v-model="addEmail.title" placeholder="TITLE" autocomplete="off" trim></b-form-input>
								</b-form-group>

								<b-form-group
									label="EMAIL"
								>
									<b-form-input v-model="addEmail.email" placeholder="ADMIN EMAIL" autocomplete="off" type="email" trim></b-form-input>
								</b-form-group>

								<b-form-group
									label="DESCRIPTION"
								>
									<b-form-textarea
										v-model="addEmail.description"
										placeholder="Describe email..."
										rows="3"
									></b-form-textarea>
								</b-form-group>
								
					            <b-col sm=12 class="text-right">
					              <b-button @click="uploadEmail" class="btn-blue">Upload</b-button>
					            </b-col>
					          </b-card>
					        </b-collapse>
					        
					    </b-col>
						
						<b-col sm=12 class="table-responsive padding" v-if="!sortingTable.length">
				            <table class="table table-hover text-center with-grid" style="width: 100%!important">
				              <thead>
				                <tr>
				                  <th v-for="f in infields" scope="col">{{ f }}</th>
				                  <th><b-button v-b-toggle.collapse-6 class="m-1">Add email</b-button></th>
				                </tr>
				              </thead>
				              <tbody>
				              	<tr >
				                  <th scope="row">Main</th>
				               	  <td>In the creation</td>
				               	  <td>Admin email</td>

				               	  <td>This is the default email to be notified and can't be deleted</td>

				               	  <td>admin@urbancitydesigns.com</td>

				               	  <td>&nbsp;</td>
				                </tr>
				                <tr v-for="(em, index) in emails">
				                  <th scope="row">#{{ index+1 }}</th>
				               	  <td>{{ em.fdated }}</td>
				               	  <td class="text-center"> {{ em.title }} </td>

				               	  <td>{{ em.description }}</td>

				               	  <td>{{ em.email }}</td>

				               	  <td class="text-center"><button type="button" @click="deleteEmail(em.id)" class="btn-flat"><i class="fas fa-times"></i></button></td>
				                </tr>
				              </tbody>
				            </table>
				        </b-col>

				        <b-col sm=12 class="table-responsive padding" v-else>
				            <table class="table table-hover text-center with-grid" style="width: 100%!important">
				              <thead>
				                <tr>
				                  <th v-for="f in infields" scope="col">{{ f }}</th>
				                  <th><b-button v-b-toggle.collapse-6 class="m-1">Add email</b-button></th>
				                </tr>
				              </thead>
				              <tbody>
			
				                <tr v-for="(em,index) in sortingTable">
				                  <th scope="row">#{{ index+1 }}</th>
				               	  <td>{{ em.fdated }}</td>
				               	  <td class="text-center"> {{ em.title }} </td>

				               	  <td>{{ em.description }}</td>

				               	  <td>{{ em.email }}</td>

				               	  <td class="text-center"><button type="button" @click="deleteEmail(em.id)" class="btn-flat"><i class="fas fa-times"></i></button></td>
				                </tr>
				              </tbody>
				            </table>
				        </b-col>
					</b-row>
				</b-tab>

				<b-tab title="EULA">
					<b-container fluid>
						<b-row>
							<b-col>
								<h5>End-User License Agreement</h5>
							</b-col>
						</b-row>
						<b-row>
							<b-col sm=6 md=2>
								<b-button class="btn btn-blue padding" @click="updateEULA">UPDATE EULA</b-button>
							</b-col>

							<b-col sm=6 md=2>
								<b-button class="btn btn-flat padding" v-b-modal.modal-eula @click="openEULA">CURRENT EULA</b-button>

								<b-modal id="modal-eula" title="Current EULA">
									<div id="eula-display" class="golden-borderOnly display-eula"></div>
								</b-modal>
							</b-col>

							<b-col sm=12 md=8>
								<p class="grey-text">If you update this EULA, all the user will need to accept again to keep using the Urban City Deisgns tools.</p>
							</b-col>
						</b-row>
						<b-row>
							<b-col>
								<div id="eula-editor" class="golden-borderOnly"></div>
							</b-col>
						</b-row>
					</b-container>
				</b-tab>

			</b-tabs>
		</b-container>
	 </div> `,
	data() {
		return {
			vd: new VueDiana(),
	        tabs: [],
	        tabCounter: 0,

	        cards: [
	        	{file: null, img: "media/cards/0.webp"},
	        	{file: null, img: "media/cards/1.webp"},
	        	{file: null, img: "media/cards/2.webp"},
	        	{file: null, img: "media/cards/3.webp"}
	        ],

	        catalogOpt: [
	        	{text: "Select category", value: null},
	        	{text: "Coastline", value: 1},
	        	{text: "Progressive", value: 2},
	        	{text: "Timeless", value: 3},
	        	{text: "Traditional", value: 4},
	        	{text: "Geometric", value: 5},
	        ],

	        catalog: {
	        	file: null,
	        	section: null,
	        	img: null
	        },

	        video: {
	        	file: null,
	        	
	        },

	        infields: ["","DATE","TITLE","DESCRIPTION","EMAIL"],
	        emails: [],
	        sortingTable: [],

	        sortEmail: "",

	        addEmail: {},

			errors: [],
			
			editor: null,
			display: null,
			currentEula: null,
			
			vd: new VueDiana(),
	    }
	},
	created(){
		this.loadCarousel();
		this.loadCards();
		this.loadEmails();
		this.$root.$data.componentTitle = "App configuration";
		
	},
	mounted(){

		this.editor = new Quill('#eula-editor');
	},
	update(){

	},
	methods: {
		openEULA(){
			var main = this;

		    axios({
		        method: 'get',
		        url: 'lpage?g=eula',
		    }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
							//main.currentEula = new Quill('#eula-display');
							document.getElementById('eula-display').innerHTML = "<p>"+response.data.info[0].content+"</p>";
							//main.currentEula.setText(response.data.info[0].content.replace(/<\s*\/?br\s*[\/]?>/gi, "\n"));

							//main.currentEula.disable();
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
						   main.vd.toastError();
						   main.currentEula = true;
		                 break;
		               }
		             break;
		             
		             default:
					   main.vd.toastError();
					   main.currentEula = true;
		             break;
		           }
		    }).catch(function (response) {
		          //handle error
		          console.log(response);
		    });
		},
		updateEULA(){
			if(confirm("Are you sure about update EULA?")){
				let main = this;
				NProgress.start();

				var updateForm = new FormData();

				updateForm.append("content",main.editor.getText());
				updateForm.append("request","updateEULA");
				updateForm.append("method","post");

				axios.post('lpage',updateForm,{
					headers: {
						'content-type': 'multipart/form-data'
					}
				}).then(function (response) {
					//handle success
					console.log(response);
					console.log(response.data);

					let call = response.data.call;

					switch(call){
						case true:
							main.vd.toastSuccess();
						break;

						case false:
							switch(response.data.errors){
								default:
									main.vd.toastError();
								break;
							}
						break;
						
						default:
							main.vd.toastError();
						break;
					}
				}).catch(function(response) {
					//handle error
					console.log(response);
				}).then(function(){
					NProgress.done();
				});

			}
		},
		sortEmails(e){
			let main = this;
			this.sortingTable = this.emails.filter(function(index) {
				
				if(index.email.toLowerCase().match(main.sortEmail.toLowerCase()) || index.fdated.toLowerCase().match(main.sortEmail.toLowerCase())){
					return index;
				}
				
			});

			if(this.sortEmail == ""){
				this.sortingTable = [];
			}
		},
		uploadEmail(){
			let main = this;
		    NProgress.start();

		    var updateForm = new FormData();

			for(var input in this.addEmail){
				updateForm.append(input,this.addEmail[input]);
			}

		    updateForm.append("request","addEmail");
		    updateForm.append("method","post");

		    axios.post('lpage',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
	            //handle success
	            console.log(response);
	            console.log(response.data);

	            let call = response.data.call;

	             switch(call){

	               case true:
	                 main.vd.toastSuccess();
	               break;

	               case false:
	                 switch(response.data.errors){
                 		case "NotEnoughForm":
                 			main.vd.toastError("","Fill at least email field");
                 		break;

                 		case "InvalidEmail":
                 			main.vd.toastError("","Invalid email format");
                 		break;

	                   default:
	                     main.vd.toastError();
	                   break;
	                 }
	               break;
	               
	               default:
	                 main.vd.toastError();
	               break;
	             }
	        }).catch(function (response) {
	            //handle error
	            console.log(response);
	        }).then(function () {
	          NProgress.done();
	        });
		},
		deleteEmail(id){
			let main = this;
	     	NProgress.start();

	      	var updateForm = new FormData();

			updateForm.append("id",id);

			updateForm.append("request","deleteEmail");
			updateForm.append("method","post");

			axios.post('lpage',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		             switch(call){

		               case true:
		                 main.vd.toastSuccess();
		               break;

		               case false:
		                 switch(response.data.errors){

		                   default:
		                     main.vd.toastError();
		                   break;
		                 }
		               break;
		               
		               default:
		                 main.vd.toastError();
		               break;
		             }
		    }).catch(function (response) {
				//handle error
				console.log(response);
		    }).then(function () {
		    	NProgress.done();
		    });
		},
		loadEmails(){
			var main = this;

		    axios({
		        method: 'get',
		        url: 'lpage?g=emails',
		    }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  main.emails = response.data.info;
		               }else{
		                  main.emails = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		    }).catch(function (response) {
		          //handle error
		          console.log(response);
		    });
		},
		uploadCatalog(){
			let main = this;
		    NProgress.start();

		    var updateForm = new FormData();
		    
		    updateForm.append("catalog",this.catalog.file);
		    updateForm.append("img",this.catalog.img);
		    updateForm.append("category",this.catalog.section);

		    updateForm.append("request","catalog-update");
		    updateForm.append("method","post");

		    axios.post('lpage',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
	            //handle success
	            console.log(response);
	            console.log(response.data);

	            let call = response.data.call;

	             switch(call){

	               case true:
	                 main.vd.toastSuccess();
	               break;

	               case false:
	                 switch(response.data.errors){
                 		case "NotEnoughForm":
                 			main.vd.toastError("","Select a video to upload");
                 		break;

	                   default:
	                     main.vd.toastError();
	                   break;
	                 }
	               break;
	               
	               default:
	                 main.vd.toastError();
	               break;
	             }
	        }).catch(function (response) {
	            //handle error
	            console.log(response);
	        }).then(function () {
	          NProgress.done();
	        });
		},
		uploadVideo(){
			let main = this;
		    NProgress.start();

		    var updateForm = new FormData();
		    
		    updateForm.append("video",this.video.file);

		    updateForm.append("request","v-update");
		    updateForm.append("method","post");

		    axios.post('lpage',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
	            //handle success
	            console.log(response);
	            console.log(response.data);

	            let call = response.data.call;

	             switch(call){

	               case true:
	                 main.vd.toastSuccess();
	               break;

	               case false:
	                 switch(response.data.errors){
                 		case "NotEnoughForm":
                 			main.vd.toastError("","Select a video to upload");
                 		break;

	                   default:
	                     main.vd.toastError();
	                   break;
	                 }
	               break;
	               
	               default:
	                 main.vd.toastError();
	               break;
	             }
	        }).catch(function (response) {
	            //handle error
	            console.log(response);
	        }).then(function () {
	          NProgress.done();
	        });
		},
		uploadCards(){
			let main = this;
		    NProgress.start();

		    var updateForm = new FormData();
		    
		    updateForm.append("collections",this.cards[0].file);
		    updateForm.append("materials",this.cards[1].file);
		    updateForm.append("shop",this.cards[2].file);
		    updateForm.append("availability",this.cards[3].file);

		    updateForm.append("request","col-update");
		    updateForm.append("method","post");

		    axios.post('lpage',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
	            //handle success
	            console.log(response);
	            console.log(response.data);

	            let call = response.data.call;

	             switch(call){

	               case true:
	                 main.vd.toastSuccess();
	               break;

	               case false:
	                 switch(response.data.errors){
	                   default:
	                     main.vd.toastError();
	                   break;
	                 }
	               break;
	               
	               default:
	                 main.vd.toastError();
	               break;
	             }
	        }).catch(function (response) {
	            //handle error
	            console.log(response);
	        }).then(function () {
	          NProgress.done();
	        });
		},
		loadCards(){
			var main = this;
		    NProgress.start();

		    axios({
		        method: 'get',
		        url: 'lpage?g=cards',
		    }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  main.cards = response.data.info;
		               }else{
		                  main.cards = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		    }).catch(function (response) {
		          //handle error
		          console.log(response);
		    }).then(function () {
		        NProgress.done();
		    });
		},
		displayImg(e,i){
			try{
		        this.cards[i].file = e.target.files[0];

		        this.cards[i].img = URL.createObjectURL(this.cards[i].file);
		    }catch(ex){}
		},
		setImage(e,i){
			console.log(i);
			try{
		        this.tabs[i].file = e.target.files[0];

		        this.tabs[i].img = URL.createObjectURL(this.tabs[i].file);
		    }catch(ex){}
		},
		uploadSlide(i){

		      let main = this;
		      NProgress.start();

		      var updateForm = new FormData();

		      for(var input in this.tabs[i]){
		        updateForm.append(input,this.tabs[i][input]);
		      }

		      updateForm.append("file",this.tabs[i].file);

		      updateForm.append("request","add");
		      updateForm.append("method","post");

		      axios.post('lpage',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		      }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errors = [];

		             switch(call){

		               case true:
		                 main.vd.toastSuccess();
		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "NoSlide":
		                     main.errors.push({message:"Is needed at least an image"});
		                   break; 

		                   default:
		                     main.vd.toastError();
		                   break;
		                 }
		               break;
		               
		               default:
		                 main.vd.toastError();
		               break;
		             }
		        }).catch(function (response) {
		            //handle error
		            console.log(response);
		        }).then(function () {
		          NProgress.done();
		        });
		},
		loadCarousel(){
			  var main = this;
		      NProgress.start();
		      main.errors = [];
		      axios({
		        method: 'get',
		        url: 'lpage?g=carousel',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  main.tabs = response.data.info;
		               }else{
		                  main.tabs = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        NProgress.done();
		      }); 
		},
		closeTab(x) {
			console.log(x);
			if(this.tabs[x].id != null){

				if(confirm("Are you sure to delete that slide?")){
					let main = this;
			     	NProgress.start();

			      	var updateForm = new FormData();

					updateForm.append("id",this.tabs[x].id);

					updateForm.append("request","delete");
					updateForm.append("method","post");

					axios.post('lpage',updateForm,{
				        headers: {
				           'content-type': 'multipart/form-data'
				        }
				    }).then(function (response) {
				            //handle success
				            console.log(response);
				            console.log(response.data);

				            let call = response.data.call;

				             switch(call){

				               case true:
				                 main.vd.toastSuccess();
				                 main.tabs.splice(x, 1);
				               break;

				               case false:
				                 switch(response.data.errors){

				                   default:
				                     main.vd.toastError();
				                   break;
				                 }
				               break;
				               
				               default:
				                 main.vd.toastError();
				               break;
				             }
				    }).catch(function (response) {
						//handle error
						console.log(response);
				    }).then(function () {
				    	NProgress.done();
				    });

				}
				
			}else{
				this.tabs.splice(x, 1);
			}
		    
		},
		newTab() {
			var carousel = {
				id: null,
				title: "",
				content: "",
				file: null,
				img: "media/img/asset-wide.png",
				dated: null
			}

			this.tabs.push(carousel);
		}
	}
});
