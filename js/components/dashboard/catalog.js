const CatalogComponent = Vue.component("catalog",{
	template: 
	`
		<div>
			<b-container fluid v-if="!catalogs.length">
				<b-row class='padding'>
		          <b-col>
		            <h4 style="color: #1E2446;" class="font-boldy elmessiri gold-flat-text">Catalog</h4>
		          </b-col>
		        </b-row>
				<b-row class='padding'>
					<b-col class="text-center" >
						<a :href="catalogs[0].pdf"></a> <b-img :src="catalogs[0].img" fluid></b-img>
						<h4>Coastline</h4> 
					</b-col>

					<b-col class="text-center" @click="openCatalog">
					</b-col>

					<b-col class="text-center" @click="openCatalog">
					</b-col>
				</b-row>

				<b-row>
					<b-col class="text-center" @click="openCatalog">
					</b-col>

					<b-col class="text-center" @click="openCatalog">
					</b-col>
				</b-row>
			</b-container>
		</div>
	  `,
	data() {
		return {
			catalogs: [],
		}
	},
	created(){
	  this.loadCatalog();

	},
	methods: {
		openCatalog(url){
			console.log("Click")
			window.open(url);
		},
	}
});
