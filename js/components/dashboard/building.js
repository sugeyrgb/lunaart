const BuildingComponent = Vue.component("building",{
	template: 
	`<div>
		<transition appear>
		   <b-container style="padding-top: 5%; transition: opacity .5s;">
		   		<b-col v-if=loading>
					<b-col class="text-center padding">
						<l-spinner color="luna-text-gold"></l-spinner>
					</b-col>
				</b-col>
				<b-col v-else class="text-center">
					<h5>This component has been escaped because is under development</h5>
				</b-col>
		   </b-container>
		</transition>
	 </div>
	  `,
	data() {
		return {
			loading: true,
		}
	},
	created(){
		var main = this;
		setTimeout(function(){
			main.loading = false;
		},500);
	},
	update(){

	},
	methods: {

	}
});
