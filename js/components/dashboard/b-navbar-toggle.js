
var ToggleSidenav = new CustomEvent("ToggleSidenav");

const toggleComponent = Vue.component("my-toggle",{
	template: 
	`<button @click="displayT" class="sidebar-button right"><i class="fas fa-bars"></i></button>`,
	data() {
		return {

			seen: true,
			message: "hola"
	
		}
	},

	methods: {
		displayT(e){
			this.seen = !this.seen
			console.log("Clicked")

			document.dispatchEvent(ToggleSidenav);
		}
	},

	created: function () {
		// `this` points to the vm instance
	}
});
