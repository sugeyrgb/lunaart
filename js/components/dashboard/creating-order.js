const CreatingComponent = Vue.component("creating-order",{
	props:{
		entryPoint: false, //Entry point is false when entering from my saves and true if entering by design pattern
		requestType: false, //True is order and false is just request. Why?, because I want deal with it
		item: {
			type: Object,
			default(){
				return {
					pattern: {
						title: "No pattern selected",
						preview: "media/img/asset.png",
					},
					extraMessage: "",
					build: [],
				}
			}
		}
	},
	template: 
	`<div>
		<b-container class='container-dp' style="padding-bottom: 20px;" fluid>
				<b-row>
					<b-col class="padding"><h4 class="gold-flat-text">Order information</h4></b-col>
				</b-row>
				
				<b-row>
					<b-col sm=12 md=6>
						<b-row>
							<b-col sm=12 class='float-right'>
								<p class="no-margin-bottom padding-top">Client name: </p>
								<b-card no-body>
									<div class="grey-luna" style="height: 38px; padding-top:5px;padding-left:5px;">
										{{ item.companyClient }}
									</div>
								</b-card>
							</b-col>

							<b-col sm=12 class='float-right'>
								<p class="no-margin-bottom padding-top">Sheet quantity: </p>
								<b-card no-body>
									<div class="grey-luna clickable" style="height: 38px; padding-top:5px;padding-left:5px;" v-b-modal.modal-calc>
										{{ details.quantity }}
									</div>
								</b-card>
							</b-col>

							<b-col sm=12>
								<p class="no-margin-bottom padding-top">Project ZIP code: </p>
								<b-form-group
									description="Zip code where the project should be send"
								>
									<b-form-input
										required
										placeholder="Ex. 22204"
										v-model="item.zp"
										:state="zpState"
										type="number"
									></b-form-input>
								</b-form-group>
							</b-col>

							<b-col sm=12 class='float-right'>
								<a href="#" target="_blank" class="gold-flat-text">Download Spect Sheet</a>
							</b-col>
						</b-row>

						<b-modal id="modal-calc" centered hide-footer>
							<b-container fluid>
								<b-row>
									<b-col cols=12 class='float-right'>
										<p>DESIGN SPECS: </p>
										<b-card no-body>
											<div class="grey-luna" style="height: 38px; padding-top:5px;padding-left:5px;">
												{{ item.pattern.dp.width+'\" x '+item.pattern.dp.height }}"
											</div>
										</b-card>
									</b-col>

									<b-col cols=12 class='float-right'>
										<p>1 SHEET COVERS: </p>
										<b-card no-body>
											<div class="grey-luna" style="height: 38px; padding-top:5px;padding-left:5px;">
												{{ item.pattern.dp.sc }} <span class="gold-flat-text">S/F</span>
											</div>
										</b-card>
									</b-col>
								</b-row>

								<b-tabs content-class="mt-3">
									<b-tab title="I NEED" active>
										<b-row>
											<b-col>
												<b-form-group
													description="Square feet"
												>
													<b-form-input
													required
													placeholder="\'"
													@input="iNeedInput($event)"
													></b-form-input>
												</b-form-group>
											</b-col>
										</b-row>
									</b-tab>
									<b-tab title="CALCULATE YOUR AREA">
										<b-row>
											<p> </p>
											<b-col>
												<b-form-group
													label="HEIGHT"
													description="Feet"
												>
													<b-form-input
													placeholder="\'"
													@input="calcInput"
													v-model="heightFeet"
													></b-form-input>
												</b-form-group>
											</b-col>

											<b-col>
												<b-form-group
													label="&nbsp;"
													description="Inch"
												>
													<b-form-input
													placeholder="&quot;"
													@input="calcInput"
													v-model="heightInch"
													></b-form-input>
												</b-form-group>
											</b-col>
										</b-row>

										<b-row>
											<b-col>
												<b-form-group
													label="WIDTH"
													description="Feet"
												>
													<b-form-input
													placeholder="\'"
													@input="calcInput"
													v-model="widthFeet"
													></b-form-input>
												</b-form-group>
											</b-col>

											<b-col>
												<b-form-group
													label="&nbsp;"
													description="Inch"
												>
													<b-form-input
													placeholder="&quot;"
													@input="calcInput"
													v-model="widthInch"
													></b-form-input>
												</b-form-group>
											</b-col>
										</b-row>
									</b-tab>
								</b-tabs>

								<b-row>
									<b-col cols=12 class='float-right'>
										<p>SHEETS NEEDED: </p>
										<b-card no-body>
											<div class="grey-luna" style="height: 38px; padding-top:5px;padding-left:5px;">
												{{ details.quantity }} <span class="gold-flat-text">SHEETS</span>
											</div>
										</b-card>
									</b-col>
								</b-row>

								<b-row>
									<b-col offset="8" class="padding-top" >
										<b-button class="btn btn-gold full-width" @click="">OK</b-button>
									</b-col>
								</b-row>
								
							</b-container>
						</b-modal>
					</b-col>

					<b-col sm=12 md=6>
						<b-row>
							<b-col sm=6 class="text-center padding">
								<b-img fluid :src="item.pattern.preview" alt="Preview" /> 
							</b-col>
							<b-col sm=6 class="text-center padding">
								<b-img fluid :src="item.pattern.dp.imgDetail" alt="Detailed pattern"  /> 
							</b-col>
						</b-row>

						<b-col sm=12>
							<small class='gold-flat-text'>{{ item.pattern.title }}</small>
						</b-col>
					</b-col>
				</b-row>

				<b-row>
					<b-col>
						<b-form-group label="Extra details:&nbsp;" style="width: 100%;" label-for="inputR">
							<b-form-textarea
								v-model="comment"
								placeholder="Detail us something..."
								rows="3"
								max-rows="3"
								trim
							></b-form-textarea>
						</b-form-group>
					</b-col>
				</b-row>
				
				<b-row>
					<b-col>
						Materials for order:
					</b-col>	
				</b-row>

				<b-row>
					<b-col sm=2 class="padding" v-for="(m,key) in item.build">
						<b-col sm=12>
							<b-img :src="m.img" fluid class="border-gold"></b-img>
						</b-col>

						<b-col md=12>
							<b-col sm="12"><span class="gold-flat-text">Material section {{ key+1 }}</span></b-col>
							<b-col sm="12"><span class="font-boldy">Material name:</span> <p>{{ m.title }}</p></b-col>
							<b-col sm="12" v-if="m.finish">
								<span class="font-boldy">Finish</span>
								{{ m.finish.text }}
							</b-col>
						</b-col>
					</b-col>
				</b-row>

				<b-row>
					<b-col offset-sm="9">
						<b-col>
							<button v-if="requestType" class="luna-gold white-text" @click="submitOrder">SUBMIT ORDER</button>
							<button v-else class="luna-gold white-text" @click="submitRequest">SUBMIT REQUEST</button>
						</b-col>
					</b-col>
				</b-row>
		</b-container>
	</div>
	  `,
	data() {
		return {
			cutType: null,

			cutTypeOptions: [
				{value: null, text: "Select type cut"},
				{value: 1, text: "Water Jet"},
				{value: 2, text: "Cut by hand"},
				{value: 3, text: "Mix cut"},
				{value: 4, text: "Nominal dimension"},
				{value: 5, text: "Real dimension"},
			],

			sendOrder: {
				companyClient: this.$root.$data.session.id,
			},

			details: {
				price: null,
				promoCode: "",
				quantity: 1,
				sf: 0,
				sfNeeded: 0,
				sfCovered: 0
			},

			comment: null,

			heightFeet: null,
			widthFeet: null,

			heightInch: null,
			widthInch: null,

			zpState: true,

			vd: new VueDiana()
		}
	},
	created(){
		for(var key in this.item.build){
			this.item.build[key].finish = this.item.pattern.finish[key]
		}

		console.log(this.sendOrder);
		console.log(this.item);

		
		this.details.sfNeeded = Math.ceil(this.item.pattern.dp.sc);

		this.item.companyClient = this.$root.$data.session.name+" "+this.$root.$data.session.lastname+(this.$root.$data.session.business != "" ? " ("+this.$root.$data.session.business+")" : "");
	},
	mounted(){
		console.log(this.sendOrder);
		console.log(this.item);
	},
	update(){
		console.log(this.item);
	},
	methods: {
		iNeedInput(event){	
			console.log(event);
			var main = this;
			var sheetCovers = main.item.pattern.dp.sc;

			main.details.quantity = Math.ceil(event/sheetCovers);

			main.details.sfNeeded = Math.ceil(event * sheetCovers);
		},
		calcInput(){
			var main = this;
				main.details.quantity = 0;
			var sheetCovers = main.item.pattern.dp.sc;
			
			if(main.heightFeet != null && main.widthFeet != null && main.heightInch != null && main.widthInch != null){
				//main.heightFeet += main.heightInch/12;
				//main.widthFeet += main.widthInch/12;

				var width = parseFloat(main.widthFeet);
				var height = parseFloat(main.heightFeet);

					height += parseFloat(main.heightInch/12);
					width += parseFloat(main.widthInch/12);

				main.details.quantity = Math.ceil((width * height)/sheetCovers);
				main.details.sfNeeded = Math.ceil((width * height));

				console.log(main.heightFeet,main.widthFeet,main.heightInch,main.widthInch,height,width);
			}

		},
		createSpecPage(){
			if(this.$root.$data.cart.display.id != null){
				window.open("spec-page?custom="+this.item.pattern.id);
			}
		},
		submitRequest(){
			NProgress.start();	

			var main = this;

			var submitForm = new FormData();

			console.log(this.item);

			this.item.client = this.$root.$data.session.id;

			this.item.cutType = this.cutType;

			if(this.entryPoint){

				var ImageURL = this.item.pattern.preview;
				var block = ImageURL.split(";");

				var contentType = block[0].split(":")[1];

				var realData = block[1].split(",")[1];

				var blob = main.vd.b64toBlob(realData, contentType);
				
				submitForm.append("original", this.item.yourimage.original);
				submitForm.append("canvas", this.item.yourimage.canvas);
				submitForm.append("preview", blob);
				//Im
				submitForm.append("imgType",true);
			}else{
				submitForm.append("imgType",false);
				submitForm.append("preview", this.item.pattern.preview);
			}
			
			if(this.item.zp == null || this.item.zp == "" || this.item.zp == "undefined"){
				this.zpState = false;
				this.vd.toastNotEnough();
				NProgress.done();
				return false;
			}

			this.item.details = this.details;

			submitForm.append("zp",this.item.zp);
			submitForm.append("comment",this.comment);
		    submitForm.append("request","add");
		    submitForm.append("method","post");

		    //Here apennds stringified all content
		    submitForm.append("form",JSON.stringify(this.item));

			axios.post('my-request',submitForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errors = [];

		             switch(call){

		               case true:
						 main.vd.toastConfirming(title="",body="WE WILL SEND YOUR REQUEST TO YOUR NEAREST DEALER AND WILL CONTACT YOU SOON",okText="UNDERSTOOD");
						 main.$root.updateRoute("/my-request");
		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "EmailExist":
								main.vd.toastError();
		                   break; 
		                 }
		               break;
		               
		               default:
		                 main.vd.toastError();
		               break;
		             }
		    }).catch(function (response) {
				//handle error
				console.log(response);
		    }).then(function () {
				NProgress.done();
			});
		},
		submitOrder(){
				
			NProgress.start();	

			var main = this;

			var submitForm = new FormData();

			console.log(this.item);

			this.item.client = this.$root.$data.session.id;

			this.item.cutType = this.cutType;

			var ImageURL = this.item.pattern.preview;

			var block = ImageURL.split(";");

			var contentType = block[0].split(":")[1];

			var realData = block[1].split(",")[1];

			var blob = main.vd.b64toBlob(realData, contentType);
			
			submitForm.append("original", this.item.yourimage.original);
			submitForm.append("canvas", this.item.yourimage.canvas);
			submitForm.append("preview", blob);
		    submitForm.append("request","add");
		    submitForm.append("method","post");

		    //Here apennds stringified all content
		    submitForm.append("form",JSON.stringify(this.item));

			axios.post('orders',submitForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errors = [];

		             switch(call){

		               case true:
		                 main.vd.toastSuccess("","Thanks for your order");
		                 main.$root.updateRoute("/dashboard");
		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "EmailExist":
		                     main.errors.push({message:"That email is already in use"});
		                   break; 
		                 }
		               break;
		               
		               default:
		                 main.vd.toastError();
		               break;
		             }
		    }).catch(function (response) {
		            //handle error
		            console.log(response);
		    }).then(function () {

			        NProgress.done();
			});
		}
	}
});
