const sessionComponent = Vue.component("session-form",{
	template: 
	`
		<b-tabs class="nav-justified" content-class="mt-3">
			<b-tab title="Login" active>

				<login-form></login-form>

			</b-tab>
			<b-tab title="Sign up" extra-toggle-classes='luna-gold'>
				<signup-form :id="formId" :contact="contacting"></signup-form>
			</b-tab>
		</b-tabs>
	  `,
	data() {
		return {
			formId: "signupForm",
			contacting: false
		}
	},
	mounted(){
		
	},
	methods: {
		resetSign(){
			console.log("reset")
		}
	}
});
