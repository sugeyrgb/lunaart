const myServer = "";
var reload = new CustomEvent("ReloadEvent",{});
var vd = new VueDiana();

Vue.use(VueViewer.default,{
  defaultOptions: { "button": false,"navbar": false,"title": false, "toolbar": false, "url": "data-source", "zoomIn": 4 }
});

Vue.use(window.vuelidate.default)
const { required, minLength } = window.validators

function done(){
  vd.toastSuccess();
}

function nope(){
  vd.toastError();
}

const routes = {
  '/': HomeComponent,
  '/collections': lCollectionComponent,
  '/materials': lMaterialComponent,
  '/about': AboutComponent,
  '/contact': ContactComponent,
  '/gallery': lGalleryComponent,
  '/forgot': ForgotComponent,
  '/eula': EULAComponent,
  '/catalog': CatalogComponent,
  '404': NotFound
}

/*
const routes = {
  '/ucd/': HomeComponent,
  '/collections': BuildingComponent,
  '/materials': BuildingComponent,
  '/about': AboutComponent,
  '/contact': BuildingComponent,
  '/gallery': BuildingComponent,
  '/forgot': ForgotComponent,
  '404': NotFound
}

*/
var viewComponent = Vue.component('route-view',{
  data: function(){
    return {
       lastRoute: myServer+window.location.pathname,
       currentRoute: myServer+window.location.pathname,
       update: true
    }
  },
  created(){
    var self = this;
    document.addEventListener("ReloadEvent",function(){
      self.lastRoute = self.currentRoute;
      self.currentRoute = window.location.pathname;
      self.$forceUpdate();
      console.log("Heared: "+window.location.pathname)
    });
  },
  computed:{
    ViewComponent () {
      console.log(this.currentRoute);
      return routes[this.currentRoute] || routes['404']
    }
  },
  beforeUpdate(){

    NProgress.start();
  },
  updated(){
    NProgress.done();
  },
  beforeCreate(){
    NProgress.start();
  },
  mounted(){
    
    NProgress.done();
  },
  render (h) { 
    window.scrollTo(0,0);
    return h(this.ViewComponent) 
  }
});

new Vue({
  el: '#app',
  data(){
    return {
       session: {
        id: 0,
        status: false,
        name: null,
        lastname: null
      },

      catalogs: [],

      displayButton: false,
      count: true,
      cart: {
        display: {},
        pattern: [],
        build: [],
        details: {}
      },
      info: null,
      message: "Hi",
      vd: new VueDiana(),

      args: null,
    }
    
  },
  components: { 
    viewComponent, 
    sessionComponent,
    VueViewer
  },
  created(){

    this.loadSession();
    var main = this;
    axios({
        method: 'get',
        url: 'lpage?g=catalogs',
    }).then(function (response) {
          //handle success
          console.log(response);
          console.log(response.data);

          let call = response.data.call;

           switch(call){

             case true:
               if(response.data.hasOwnProperty("info")){
                  main.catalogs = response.data.info;
               }else{
                 main.catalogs = [];
               }
              
             break;

             default:
               vd.toastError();
             break;
           }

           main.loading = false;
    }).catch(function (response) {
          //handle error
          console.log(response);
    }).then(function () {
        NProgress.done();
        main.$forceUpdate();
    }); 
  },
  updated(){
    this.displayButton = this.cart.display.hasOwnProperty("id");
    
    if(this.count && this.displayButton != false){
      this.$refs.cartSave.openCart();
      this.count = false;
    }
  },
  mounted(){
  },
  methods: {
    loadSession(){
      let main = this;
      let sd = localStorage.getItem('sd');


      if(main.vd.isSnE(sd)){
        sd = JSON.parse(sd);
        main.session = sd.user;

      }else{
        axios.get('login?r=gs').then(function(response){
          main.session = response.data.info.user;
          main.$refs.loginModal.hide();
        }).catch(function(error){
          console.log(error);
        });
      } 
    },
    updateRoute(newUrl,args = ""){
      if(args != ""){
        this.args = args;
      }else{
        this.args = null;
      }
      history.pushState(null, null, newUrl);
      console.log("Main: "+window.location.pathname)
      document.dispatchEvent(reload);
    }
  }
});

new Vue({
  el: '#footer',
  template: `
    <footer class='my-footer'>
      <b-container fluid>

        <b-row style="padding-bottom: 5px;">
            
            <b-col sm=12 md=6 class="footer-links">
              <h4 class='gold-text'>FOLLOW</h4>
              <a href="https://www.instagram.com/urbancitydesigns/" target="_blank"><img :src="insImg" @mouseover="insImg = 'media/img/f-insta-h.png'" @mouseleave="insImg = 'media/img/f-insta-g.png'" alt="instagram" /></a>
              <a href="https://www.facebook.com/urbancitydesigns" target="_blank"><img :src="fbImg" @mouseover="fbImg = 'media/img/f-fb-h.png'" @mouseleave="fbImg = 'media/img/f-fb-g.png'" alt="facebook" /></a>
              <a href="https://www.pinterest.com.mx/ucd2907/" target="_blank"><img :src="pintImg" @mouseover="pintImg = 'media/img/f-pint-h.png'" @mouseleave="pintImg = 'media/img/f-pint-g.png'" alt="pinterest" /></a>

            </b-col>

            <b-col sm=12 md=6>
              
              <b-row>
                <b-col sm=3>
                  <ul class="list-unstyled">
                          <li>
                            <span class="luna-text-gold font-boldy">SHOP</span>
                          </li>
                          <li>
                            <a @click="updateRoute('/collections')" href="#!">Collections</a>
                          </li>
                          <li>
                            <a @click="updateRoute('/materials')" href="#!">Materials</a>
                          </li>
                          <li>
                            <a @click="updateRoute('/gallery')" href="#!">Gallery</a>
                          </li>
                        </ul>
                </b-col>

                <b-col sm=4>
                  <ul class="list-unstyled">
                          <li>
                            <span class="luna-text-gold font-boldy">HELP</span>
                          </li>
                          <li>
                            <a @click="updateRoute('/contact')" href="#!">Location</a>
                          </li>
                          <li>
                            <a @click="updateRoute('/contact')" href="#!">Contact Form</a>
                          </li>
                        </ul>
                </b-col>

                <b-col sm=4>
                  <ul class="list-unstyled">
                          <li>
                            <a @click="updateRoute('/about')" class="luna-text-gold font-boldy" style="cursor: pointer;">ABOUT US</a>
                          </li>
                        </ul>
                </b-col>
              </b-row>
              
              


            </b-col>

        </b-row>

        <b-row>
          <b-col class="text-center" style='padding-top: 25px; border-top: 1px solid #FFFFFF;'><small class="luna-text-lightGrey">Urban City Designs. All rights reserved.</small></b-col>
        </b-row>
        
      </b-container>    
    </footer>`,
      data() {
      return {
        fbImg: "media/img/f-fb-g.png",
        pintImg: "media/img/f-pint-g.png",
        insImg: "media/img/f-insta-g.png",
      }
    }, 
    created(){

    },
    methods: {
      updateRoute(newUrl,args = ""){
        if(args != ""){
          this.args = args;
        }else{
          this.args = null;
        }
        console.log(newUrl);
        history.pushState(null, null, newUrl);
        console.log("Click")
        console.log("Main: "+window.location.pathname)
        document.dispatchEvent(reload);
      }
    }
});