# Simple LunaArt Docs

This documentation only includes assets admin management part for external purpouses

# Admin area

Admin area is the section where admins (employees) can manage uber tools.
 
## Assets 

Assets section can be accessed from the admin tools on the left menu. Here you can manage the collections, materials and clean patterns to be used in the whole app.

### Materials

**Materials** tab under **assets** section is where admin can manage the avaible main material items.

![enter image description here](https://lh3.googleusercontent.com/j86FXq7uvbFF_TQ2lvsuEFzweTNx3JmKOF5CehAcSef5Qax9Zibnjr8W1vcMwRanPpcO3-pHyK_8 "Material tab")

#### Adding new material

Material item needs 3 fields to be stored on the server: *Image*, *Name* and *category*, once is saved in app it cannot be updated, only deleted by clicking on it and confirming prompt

### Pattern

Pattern tab under assets section is where admin will be able to manage *clean patterns* and full *collections*

####  Clean pattern

Clean pattern is under **pattern tab** and is accessible after clicking once on the right screen button, clicking twice will return to collections view. Clean pattern is an intern feature used to manage *Design pattern* section in user tools.

![enter image description here](https://lh3.googleusercontent.com/8caT7khoMtnBPiw9dVHr1xjiu0ZGR2rxkr5RhlbtSsytvE7Yvd71fWPLfpnkwqNJWHUaD3-hj4NL "Entering to clean pattern view")

After clicking on the toggler button you will be able to see clean pattern view.
![enter image description here](https://lh3.googleusercontent.com/ogXQFh5mbdJ00zdZML-hsiW9kcZ8yuvhdgsJ18s1TvSbvYszW3vjt3KAmV1oKK1L34KF_nrSUN_a "Clean pattern view")

#### Adding new clean pattern

After click on **Add pattern** button you will open the both menus to add a materialized pattern (Collection) and clean pattern. To add new clean pattern select *Clean pattern* tab. Clean pattern needs *name*, *display* *image*, *detailed image*, *chroma app image*, *crhomas quantity*, *price*, *width*, *height* and *collection*.

**Name**
Main name to be displayed 

**Display image**
Pattern thumbnail to be displayed
![enter image description here](https://lh3.googleusercontent.com/amNjZanPFygRWie-alqS2Ax9ZGTKV6iCSwVVUonhqwMEwiOdsCVuAHuFGgRcJRb1ySUal8trCSCb "Clean pattern example")

**Detailed image**

Clean pattern detailed image, mainly used in **Pattern customization** tool
![enter image description here](https://lh3.googleusercontent.com/bhIkmulXvcQsfU6-OrU4JbeX6Kmu-KodbKDUBsxU1yY41LJ4eFvL1q8MogI_ybGHpsmbY5Td_l-a "Detailed pattern image")

**Chroma app image**
This is an intern image to be used in **Pattern customization** tool, it's important so the app can be able to search sections and replace it automaticaly with materials. If the *Pattern customization* does not replace right the colors, check that the color value has been set right.

![enter image description here](https://lh3.googleusercontent.com/7OXVcn5cryMqh31h8iV10_cKkWoR0jKyssaElB6K8zOmpREBZvifa9dYjA1YGmDgz05pETs_rbY_ "Chromatized pattern")

	Section 1 is color #3cff00
	Section 2 is color #0024ff
	Section 3 is color #ff0000
	Section 4 is color #ff00fc
	Section 5 is color #00fff6

![enter image description here](https://lh3.googleusercontent.com/-_KtdeRX6G73ASpC7pCcVuKStNJz0ULj31Rxgjd4bc255ZrlKdwqBWgmWNzhpqDoPnlNftlVqBjL "Dynamic materialization")

	This is a preview of how the Pattern customization replaces the crhomas with the selected materials by the client

It's very important to take care of the pattern delimiter lines as the right image so the app doesn't redraw with color shadows.
![enter image description here](https://lh3.googleusercontent.com/7v5ownaCGG2slTKC3_B3BKyEzBN2h1-PoZScPwjek1vGpMcq4znp7rL8wu3srvrhjCSVzJNzgmxK "Do the right, not the left")

This chroma patterns has to be premade by a designer

**Chromas quantity**
Even if the chroma colors are present in the chroma image, the app needs an explicit declaration of chromas in the presented image. Ex, this doc would need 5 as the colors to be searched

**Price**
Price to be hanlded in the app

**Width**
Widht of the pattern only inches

**Height**
Height of the pattern only inches

**Collection** 
Category where the pattern can be found

###  Materialized pattern
Materialized pattern are the items used in the *Collection* page in **Landing page** and is very importan set at least an clean pattern and material before you try to ser up a *Materialized pattern item (Collection)*

**Name**
Main name to be displayed of the collection

**Image slide**
By default theres no inputs to insert image, but can be controlled all the slides input for the collection adding or removing with the plus and remove buttons.
![enter image description here](https://lh3.googleusercontent.com/JGqMPxr09OWapXusbs5bX9V9ZmU02DxtD9qZYQ3pqci1EzYLYfhUGEYQhCNpQ-tfGENJlqjUZqS0 "Buttons for image display")

**Material for image slide**
After you add an image for the slider, you'll need set at least one material to be displayed in the slide
![enter image description here](https://lh3.googleusercontent.com/nvDL3-o8mBCgnUTf15g4lhXu5lIvZt6OPXfZouh1Fc2UkNhRNelux_cw57POPFVgmedgIwDLTync "Full example form")

**Main pattern**
Select an clean pattern to get details from it, and skip the part of set some specifications again. Is needed to have the clean pattern that you want in your library.

**Price**
Set the price to the collection

**Collection**
Set the category to be stored

