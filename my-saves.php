<?php 
	//CORS Policy declatarion

    //print_r(empty($_GET));
	
	$method = "";
	$page = true;

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':
				//Get request handler
				switch ($_GET["g"]) {
					case "oneSave":

						$materials = array();

						if(isset($_GET['customInDP'])){
							$gam = "SELECT * FROM mysaves WHERE id = ".$_GET['id'];
						}else{
							$gam = "SELECT * FROM collection WHERE id = ".$_GET['id'];
						}
						
						$api = $db->query($gam,true,true);

						if(!isset($_GET["materials"])){
							if(!empty($api["query"][0]["materials"])){
								$api["query"][0]["materials"] = json_decode($api["query"][0]["materials"],false);
							}else{
								$api["query"][0]["materials"] = explode(',',$api["query"][0]["materialsStr"]);
							}
						}else{
							$api["query"][0]["materials"] = $_GET["materials"];
						}
						
						foreach($api["query"][0]["materials"] as $key => $value){
							$api["query"][0]["materials"][$key] = trim($value);
						}

						foreach($api["query"][0]["materials"] as $key => $value) {
							$sql = "SELECT * FROM material WHERE title = '".$value."' LIMIT 1";
							$test = $db->query($sql, true,true);
							array_push($materials,$test["query"][0]);
						}

						$api["query"][0]["dp"] = $db->query("SELECT * FROM dp WHERE id = ".$api["query"][0]["dp"]);
						$api["query"][0]["dp"] = $api["query"][0]["dp"]["query"][0];

						$api["query"][0]["materials"] = $materials;
						$api["query"] = $api["query"][0];

						$lb->toClient(true,$api,false);
					break;

					case "selfSaves":
						$sql = "SELECT * FROM mysaves WHERE owner = ".$_SESSION["user"]["id"];
						
						$query = $db->query($sql, true);

						if($query["status"]){
							$lb->toClient(true,$query["query"]);
						}else{
							$lb->defaultRequest();
						}
					break;

					case "selfMaterials":
						if($lb->isAdmin()){
							$sql = "SELECT *, material.id as id FROM `material`,`mysaves` WHERE material.id = mysaves.asset AND mysaves.type = 1 AND mysaves.owner = ".$_GET['id'];
						}else{	
							$sql = "SELECT *, material.id as id FROM `material`,`mysaves` WHERE material.id = mysaves.asset AND mysaves.type = 1 AND mysaves.owner = ".$_SESSION['user']['id'];
						}

						$materialSaves = $db->query($sql,true);

						if($materialSaves["status"]){
							$lb->toClient(true,$materialSaves["query"]);
						}else{
							$lb->defaultRequest();
						}
						
					break;

					default:
						$lb->defaultRequest();
					break;
				}
				
			break;

			case 'POST':
				//Post request handler
				switch ($_POST['request']) {
					case "add":
					
						$_POST["cart"] = json_decode($_POST["cart"],true);

						extract($_POST);

						if(!empty($cart["pattern"]) || !empty($cart["build"])){
							$pattern = $cart["pattern"];
							foreach($pattern as $key => $value){
								$imgIndex = $pattern[$key]['imgIndex'];
								$materialsArr = explode(",",$pattern[$key]['displayArray'][$imgIndex]);
								$newPattern = "INSERT INTO `mysaves` (`type`, 
																	  `asset`, 
																	  `custom`, 
																	  `materials`, 
																	  `dp`,
																	  `coment`, 
																	  `owner`, 
																	  `img`) 
															   VALUES (0,
															   		  ".$pattern[$key]['id']." , 
																	  ".($pattern[$key]['id'] == 'custom' ? 1 : 0).",
																	  '".json_encode($materialsArr)."', 
																	  ".$pattern[$key]['dp'].",
																	  'Default text', 
																	  ".$_SESSION['user']['id'].", 
																	  '".$pattern[$key]['img']."')";

								$thisQuery = $db->query($newPattern,false);

								if(!$thisQuery["status"]){
									$lb->defaultQuery();
									return false;
								}
							}

							$material = $cart["build"];
							foreach($material as $key => $value){
								$newMaterial = "INSERT INTO `mysaves` (`title`,
																	  `type`, 
																	  `asset`, 
																	  `custom`, 
																	  `materials`, 
																	  `coment`, 
																	  `owner`, 
																	  `img`) 
															   VALUES ('".$material[$key]['title']."',
																   	  1,
															   		  ".$material[$key]['id']." , 
																	  0,
																	  NULL, 
																	  'Default text', 
																	  ".$_SESSION['user']['id'].", 
																	  '".$material[$key]['img']."')";
								$thisQuery = $db->query($newMaterial,false);

								if(!$thisQuery["status"]){
									$lb->defaultQuery();
									return false;
								}
							}

							$lb->successRequest();
						}

					break;
					
					case "delete":
						extract($_POST);
						if($lb->isAdmin()){						
							$deleteQuery = "DELETE FROM mysaves WHERE id =".$id;
						}else{						
							$deleteQuery = "DELETE FROM mysaves WHERE id =".$id." AND owner = ".$_SESSION["user"]["id"];
						}

						$query = $db->query($deleteQuery,false);

						if($query["status"]){
							$lb->successRequest();
						}else{
							$lb->defaultRequest();
						}
					break;

					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT
				$lb->defaultRequest();
			break;

			case 'DELETE':
				//Post handled like Delete
				$lb->defaultRequest();
			break;
			
			default:
				echo "{'error':'Bad server request'}";
			break;
		}
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	

	//Third FOM MySQL Server connection 

	if(!$page){
		$FriendofMySQL = new FriendofMySQL("root","login",$method); 
		if($FriendofMySQL->callback != "false"){ 
			echo json_encode($FriendofMySQL->callback); 
		}else{ 
			echo "{'error':false}"; 
		} 
	}
	

?>