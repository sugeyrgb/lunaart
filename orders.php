<?php 

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':
				
				switch ($_GET["g"]) {
					case 'a':
						$lb->power_session_start();
						if($lb->isAdmin()){
							$sql = "SELECT *, LPAD(`number`, 6, '0') as `number`, orders.id as id, user.id as uid, DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated, DATE_FORMAT(orders.receivedDate, '%m/%d/%y') as rdated, DATE_FORMAT(orders.approvalDate, '%m/%d/%y') as adated FROM orders, user WHERE orders.client = user.id  ORDER BY orders.dated DESC LIMIT 10";

							$orders = $db->query($sql,true);

							$lb->toClient(true,$orders["query"]);
						}else{
							$lb->defaultPermission();
						}
					break;

					case "p":
						$lb->power_session_start();

						$sql = "SELECT *, LPAD(`number`, 6, '0') as `number`, orders.id as id, DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated, DATE_FORMAT(orders.receivedDate, '%m/%d/%y') as rdated, DATE_FORMAT(orders.approvalDate, '%m/%d/%y') as adated FROM orders WHERE orders.client = ".$_SESSION['user']['id']."  ORDER BY orders.dated DESC";

						$orders = $db->query($sql,true,true);

						$lb->toClient(true,$orders["query"]);
					break;

					case 'o':
						$lb->power_session_start();

						if(!isset($_SESSION["user"])){
							$lb->toClient(false);
							return false;
						}

						if(!isset($_GET['n'])){
							$_GET['n'] = 0;
						}

						if($lb->isAdmin()){

							$setR = "SELECT received FROM orders WHERE `number` = ".$_GET['n'];

							$queryR = $db->query($setR,true);

							if($queryR["query"][0]["received"] == NULL){
								$updR = "UPDATE orders SET received = ".$_SESSION["user"]["id"].", receivedDate = CURRENT_TIMESTAMP WHERE `number` = ".$_GET['n'];

								$db->query($updR,false,true);

							}

							$sql = "SELECT *, LPAD(`number`, 6, '0') as `number`, orders.id as id, user.id as uid, DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated, DATE_FORMAT(orders.receivedDate, '%m/%d/%y') as rdated, DATE_FORMAT(orders.approvalDate, '%m/%d/%y') as adated  FROM orders, user WHERE orders.client = user.id AND orders.number = ".$_GET['n']." LIMIT 1";

							$orders = $db->query($sql,true,true);
							$admin = $db->query("SELECT id,name,lastname,email FROM user WHERE id = ".$orders["query"][0]["received"]);
							$client = $db->query("SELECT id,name,lastname,email,business FROM user WHERE id = ".$orders["query"][0]["client"]);

							$orders["query"][0]["admin"] = $admin["query"][0];
							$orders["query"][0]["clientInfo"] = $client["query"][0];

							if(!empty($orders["query"][0])){
								$orders["query"][0]["content"] = json_decode($orders["query"][0]["content"],true);
							}

							$lb->toClient(true,$orders["query"]);
						}else{
							$sql = "SELECT *, LPAD(`number`, 6, '0') as `number`, orders.id as id, user.id as uid, DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated FROM orders, user WHERE orders.client = '".$_SESSION['user']['id']."' AND orders.number = ".$_GET['n']." LIMIT 1";
							$orders = $db->query($sql,true);

							$admin = $db->query("SELECT id,name,lastname,email FROM user WHERE id = ".$orders["query"][0]["received"]);
			
							$orders["query"][0]["admin"] = ($admin["num_rows"] != 0 ? $admin["query"][0] : null);
							$orders["query"][0]["clientInfo"] = array();

							if(!empty($orders["query"][0])){
								$orders["query"][0]["content"] = json_decode($orders["query"][0]["content"],true);
							}

							$lb->toClient(true,$orders["query"]);
						}
					break;

					case "uo":
						$lb->power_session_start();
						if($lb->isAdmin()){

							$sql = "SELECT *, LPAD(orders.`number`, 6, '0') as `number`, DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated, DATE_FORMAT(orders.receivedDate, '%m/%d/%y') as rdated, DATE_FORMAT(orders.approvalDate, '%m/%d/%y') as adated FROM orders WHERE orders.client = ".$_GET["id"];
							
							$orders = $db->query($sql,true);

							foreach ($orders["query"] as $key => $value) {
								$invQ = "SELECT * FROM invoice WHERE `number` = ".$orders["query"][$key]["number"]." LIMIT 1";

								$invTry = $db->query($invQ,true);

								$orders["query"][$key]["orderUrl"] = "files/".$lb->encrypt($_SESSION["user"]["id"])."/order/".$lb->encrypt($orders["query"][$key]["id"]);

								$orders["query"][$key]["content"] = json_decode($orders["query"][$key]["content"],true);

								$orders["query"][$key]["previewImages"] = array();

								if($orders["query"][$key]["previewGallery"]){
									foreach(glob($orders["query"][$key]["orderUrl"].'/gallery/*.*') as $file) {
										
										array_push($orders["query"][$key]["previewImages"], $file);
									}	
								}

								if(array_key_exists(0,$invTry["query"]) ){
									$orders["query"][$key]["invoice"] = $invTry["query"][0];
								}else{
									$orders["query"][$key]["invoice"] = NULL;
								}

							}

							$lb->toClient(true,$orders["query"]);
						}else{
							$lb->defaultPermission();
						}
					break;	

					case 'm':
						$lb->power_session_start();

						if(isset($_SESSION["user"]["id"])){
							$sql = "SELECT *, LPAD(orders.`number`, 6, '0') as `number`, DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated, DATE_FORMAT(orders.receivedDate, '%m/%d/%y') as rdated, DATE_FORMAT(orders.approvalDate, '%m/%d/%y') as adated FROM orders WHERE orders.client = ".$_SESSION["user"]["id"]. " ORDER BY orders.dated DESC";

							$orders = $db->query($sql,true);

							foreach ($orders["query"] as $key => $value) {
								$invQ = "SELECT * FROM invoice WHERE `number` = ".$orders["query"][$key]["number"]." LIMIT 1";

								$invTry = $db->query($invQ,true);

								$orders["query"][$key]["orderUrl"] = "files/".$lb->encrypt($_SESSION["user"]["id"])."/order/".$lb->encrypt($orders["query"][$key]["id"]);

								$orders["query"][$key]["content"] = json_decode($orders["query"][$key]["content"],true);

								$orders["query"][$key]["previewImages"] = array();

								if($orders["query"][$key]["previewGallery"]){
									foreach(glob($orders["query"][$key]["orderUrl"].'/gallery/*.*') as $file) {
										
										array_push($orders["query"][$key]["previewImages"], $file);
									}	
								}

									

								if(array_key_exists(0,$invTry["query"]) ){
									$orders["query"][$key]["invoice"] = $invTry["query"][0];
								}else{
									$orders["query"][$key]["invoice"] = NULL;
								}

							}

							$lb->toClient(true,$orders["query"]);
						}
						
					break;

					case "as":
						$lb->power_session_start();
						
						if($lb->isAdmin()){

							$sql = "SELECT *, LPAD(`number`, 6, '0') as `number`, orders.id as id, user.id as uid, DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated, DATE_FORMAT(orders.receivedDate, '%m/%d/%y') as rdated, DATE_FORMAT(orders.approvalDate, '%m/%d/%y') as adated  FROM orders, user WHERE orders.client = user.id AND orders.number LIKE '%".$_GET['number']."%' LIMIT 10";

							$orders = $db->query($sql,true,true);

							$lb->toClient(true,$orders["query"]);
						}else{
							$lb->defaultPermission();
						}
					break;
					
					default:
						$lb->power_session_start();
						if($lb->isAdmin()){
							$sql = "SELECT *, LPAD(orders.`number`, 6, '0') as `number`,  DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated, user.id as uid, orders.id as id FROM orders,user WHERE orders.client = user.id AND ((orders.`number` LIKE '%".$_GET['g']."%') OR (CONCAT(user.name,' ',user.lastname,' ',user.business) LIKE '%".$_GET['g']."%' )) LIMIT 10";

							$list = $db->query($sql,true,true);

							if($list["status"]){
								$lb->toClient(true,$list['query']);
							}else{
								$lb->defaultQuery();
							}
						}else{
							$lb->defaultPermission();
						}					
						
					break;
				}
				

			break;

			case 'POST':
				switch ($_POST["request"]) {
					case 'add':

						$lb->power_session_start();

						$order = json_decode( $_POST["form"] , true);

						unset($order["materials"]);
						
						unset($order["pattern"]["preview"]);

						foreach ($order["build"] as $key => $value) {
							if(isset($order["pattern"]["finish"][$key])){
								$order["build"][$key]["finish"] = $order["pattern"]["finish"][$key];
							}else{
								$order["build"][$key]["finish"] = null;
							}
						}

						unset($order["pattern"]["finish"]);

						$times_hash = $lb->encrypt(time());

						if(isset($_FILES["preview"])){
							$setPreview = $lf->mf($_FILES["preview"],"files/".$lb->encrypt($_SESSION['user']['id'])."/order/".$times_hash,"preview.jpg");
						}else{
							$setPreview = $order["pattern"]["img"];
						}

						$number = $db->randomNumber(1,999999,"orders","number");

						if($setPreview["answer"]){
							$content["pattern"] = $order["pattern"];
							$content["build"] = $order["build"];
							$content["details"] = $order["details"];

							if($order["details"]["quantity"] >= 0){
								$sql = "INSERT INTO orders (timeHash, 
															`number`,
															title, 
															client, 
															received,
															previewImg, 
															udpatedDate, 
															content,
															cutType, 
															invoice, 
															approval, 
															approvalDate,
															dated)
													VALUES ('$times_hash',
															'$number',
															'Order from ".$_SESSION['user']['name']." ".$_SESSION['user']['lastname']." ".($_SESSION['user']['business'] != '' ? "( ".$_SESSION['user']['business']." )" : '' )."', 
															".$_SESSION['user']['id'].", 
															DEFAULT,
															'".$setPreview['dir_name']."', 
															CURRENT_TIMESTAMP, 
															'".json_encode($content)."', 
															'".$order['cutType']."',
															NULL, 
															0, 
															NULL, 
															CURRENT_TIMESTAMP)";

								$addOrder = $db->query($sql, false, true);

								$sqlMails = "SELECT * FROM mails";

								$mails = $db->query($sqlMails,true);

								
								$escapeMails = array("admin@urbancitydesigns.com");

								foreach ($mails["query"] as $key => $value) {
									array_push($escapeMails,$mails["query"][$key]["email"]);
 								}

								if($addOrder["status"]){

									$info = $db->query("SELECT *, LPAD(orders.`number`, 6, '0') as `number` FROM orders WHERE id = ".$addOrder["last_id"]);

									$info["query"] = $info["query"][0];


									$msg = "A client has made a new order request, this order is waiting for any admin review \n Order number: #".$info["query"]["number"]." \n Client: ".$_SESSION["user"]["name"]." ".$_SESSION["user"]["lastname"]." ".($_SESSION["user"]["business"] != '' ? '('+$_SESSION["user"]["business"].')' : '');

									foreach ($escapeMails as $key => $value) {
										$lb->sendEmail("no-reply@urbancitydesigns.com",$value,"New order",$msg);
									}

									
									$lb->toClient(true);
								}else{
									$lb->defaultQuery();
								}	
							}
						
						}else{
							$lb->toClient(false,$callback,"NotFileMoved");
						}
					break;

					case "addCustom":
						
					break;
						
					case 'update':
						$lb->power_session_start();
						$callback["waning"] = array();

						if($lb->isAdmin()){
							$_POST["order"] = json_decode($_POST["order"],true);

							extract($_POST);
							$updateGallery = array();

							$galleryFlag = 0;

							if(isset($_FILES['processGallery'])){
								foreach ($_FILES['processGallery']["name"] as $key => $value) {
									$updateGallery[$key]["name"] = $_FILES['processGallery']["name"][$key];
									$updateGallery[$key]["type"] =  $_FILES['processGallery']["type"][$key];
									$updateGallery[$key]["tmp_name"] = $_FILES['processGallery']["tmp_name"][$key];
									$updateGallery[$key]["error"] =  $_FILES['processGallery']["error"][$key];
									$updateGallery[$key]["size"] =  $_FILES['processGallery']["size"][$key];
								}

								$path = "files/".$lb->encrypt($_POST["order"]["client"])."/order/".$lb->encrypt($_POST["order"]["id"])."/gallery/";

								$files = glob($path."*");
								foreach($files as $file){ // iterate files
								  if(is_file($file))
								    unlink($file); // delete file
								}

								foreach ($updateGallery as $key => $value) {
									$lf->mf($updateGallery[$key],$path,$key.".jpg");
								}

								$galleryFlag = 1;
							}else{
								$galleryFlag = $order["previewGallery"];
							}

							

							if(isset($_FILES['invoice'])){
								
								if($lf->cf($_FILES['invoice'],"pdf")){

									$pdf = $lf->mf($_FILES['invoice'],"files/".$lb->encrypt($order['client'])."/invoices",$_FILES['invoice']["name"]);

									//$lb->log($pdf);

									$inP = "REPLACE INTO invoice (`number`, 
																	pdf, 
																	description, 
																	client, 
																	uploader, 
																	dated) 
															VALUES ('".$order['number']."',
																	'".$pdf['dir_name']."',
																	NULL,
																	'".$order['client']."',
																	'".$_SESSION['user']['id']."',
																	CURRENT_TIMESTAMP)";

									if($pdf["answer"]){
											
										$tryQ = $db->query($inP,false);

										if(!$tryQ["status"]){

											array_push($callback["waning"], "NotInvoiceUploaded");
											 
										}
										
									}
								}
							}


							/* preparing master update */
							$sql = "UPDATE `orders` 
									SET process = ".$process.",
										priority = '".$priority."',
										`comment` = '".$comment."',
										previewGallery = ".$galleryFlag."
									WHERE id = ".$order["id"];

							$tryO = $db->query($sql,false);

							if($tryO["status"]){
								$lb->toClient(true,$callback);
							}else{
								$lb->defaultQuery();
							}
							
						}

						
					break;	

					case "approval":
						$lb->power_session_start();

						$sql = "UPDATE orders SET approval = ".$_POST["option"].", approvalDate = CURRENT_TIMESTAMP WHERE number = ".$_POST["number"]." AND client = ".$_SESSION["user"]["id"];

						$approval = $db->query($sql,false);

						if($approval["status"]){
							$lb->toClient(true);
						}else{
							$lb->toClient(false,$callback,"Error");
						}
						

					break;

					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT
			break;

			case 'DELETE':
				//Post handled like Delete

			break;
			
			default:
				$lb->defaultMethod($method);
			break;
		}

		$db->close($connection);

	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	//Third FOM MySQL Server connection 
	

?>